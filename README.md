Download test data
------------------

Test data corresponds to the [Human Brain Reference
(HBR)](https://github.com/griffithlab/rnaseq_tutorial/wiki/ResourceFiles/HBR.pdf),
RNA isolated from the brains of 23 Caucasians using Ribo-Zero Gold (see [this
tutorial](https://github.com/griffithlab/rnaseq_tutorial/wiki/RNAseq-Data) for
more info). Only reads mapping to chr22 are present in the samples.

To donwload the files, use
`bash
bash scripts/download_test_data.sh
`

Salmon and STAR indexing
------------------------

To download and index a transcriptome to use as a reference for Salmon, use the
following script:
`bash
bash scripts/salmon_indexing.sh -r 35
`
The script has some options that can be changed (output directory, gencode
release, etc...). Use
`bash
bash scripts/salmon_indexing.sh -h
`
for help.

NOTE: more than 32G of RAM are required to index the human transcriptome with
the whole genome as a decoy.

Similarly, to download and index the genome for STAR, use
`bash
bash scripts/STAR_indexing.sh
`
The script, in addition of generating the indices, creates other files
needed for the SNV calling with GATK.


Testing samples
---------------

To run a sample:

`bash
python3 rna-seq-salmon.py -1 ./test_data/HBR_Rep1_ERCC-Mix2_Build37-ErccTranscripts-chr22.R1.fastq.gz \
    -2 ./test_data/HBR_Rep1_ERCC-Mix2_Build37-ErccTranscripts-chr22.R2.fastq.gz \
    -o ./test_results --sample-id HBR_Rep1
`

RNA-seq short variant discovery
-------------------------------

Based on: https://github.com/gatk-workflows/gatk4-rnaseq-germline-snps-indels

But using Hisat2 as aligner (STAR had problems in Azure because of the FIFOs).
`bash
python3 snv.py -1 ./test_data/HBR_Rep1_ERCC-Mix2_Build37-ErccTranscripts-chr22.R1.fastq.gz \
    -2 ./test_data/HBR_Rep1_ERCC-Mix2_Build37-ErccTranscripts-chr22.R2.fastq.gz \
    -o ./test_results --sample-id HBR_Rep1
`






