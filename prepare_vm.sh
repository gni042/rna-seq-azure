#!/bin/bash

echo "#### Updating Ubuntu dist..."
sudo apt update && sudo apt upgrade -y

echo "#### Installing some Ubuntu packages..."
sudo apt install -y unzip build-essential libncurses5-dev zlib1g-dev libbz2-dev liblzma-dev p7zip-full pigz
sudo apt install -y neovim python3-psutil default-jre

echo "#### Installing samtools 1.10..."
cd ~
wget https://github.com/samtools/samtools/releases/download/1.10/samtools-1.10.tar.bz2
tar -xvjf samtools-1.10.tar.bz2
rm samtools-1.10.tar.bz2
cd samtools-1.10
./configure
make
sudo mkdir /opt/samtools-1.10
sudo cp ./samtools /opt/samtools-1.10/samtools
sudo ln -s /opt/samtools-1.10/samtools /usr/local/bin/samtools-1.10
sudo ln -s /usr/local/bin/samtools-1.10 /usr/local/bin/samtools
rm -r ./samtools-1.10

echo "#### Installing bcftools..."
cd ~
wget https://github.com/samtools/bcftools/releases/download/1.11/bcftools-1.11.tar.bz2
tar -xvjf bcftools-1.11.tar.bz2
rm bcftools-1.11.tar.bz2
cd cd bcftools-1.11
./configure
make
sudo mkdir /opt/bcftools-1.11
sudo cp ./bcftools /opt/bcftools-1.11/bcftools
sudo ln -s /opt/bcftools-1.11/bcftools /usr/local/bin/bcftools-1.11
sudo ln -s /usr/local/bin/bcftools-1.11 /usr/local/bin/bcftools
cd ~
rm -r bcftools-1.11


echo "#### Installing the SRA toolkit..."
cd ~
wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.9.6/sratoolkit.2.9.6-ubuntu64.tar.gz
tar -xzvf sratoolkit.2.9.6-ubuntu64.tar.gz
rm sratoolkit.2.9.6-ubuntu64.tar.gz
sudo mkdir /opt/sratoolkit-2.9.6
sudo cp -r ./sratoolkit.2.9.6-ubuntu64/bin/vdb-config* /opt/sratoolkit-2.9.6/
sudo ln -s /opt/sratoolkit-2.9.6/vdb-config /usr/local/bin/vdb-config
sudo cp -r ./sratoolkit.2.9.6-ubuntu64/bin/prefetch* /opt/sratoolkit-2.9.6/
sudo ln -s /opt/sratoolkit-2.9.6/prefetch /usr/local/bin/prefetch
sudo cp -r ./sratoolkit.2.9.6-ubuntu64/bin/fastq-dump* /opt/sratoolkit-2.9.6/
sudo ln -s /opt/sratoolkit-2.9.6/fastq-dump /usr/local/bin/fastq-dump

echo "#### Installing gsutil..."
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
sudo apt install apt-transport-https ca-certificates gnupg
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sudo apt update && sudo apt install google-cloud-sdk


echo "#### Installing FastQC"
cd ~
wget https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip
unzip fastqc_v0.11.9.zip 
rm fastqc_v0.11.9.zip 
sudo mkdir /opt/fastqc-0.11.9
sudo cp -r FastQC/* /opt/fastqc-0.11.9/
sudo chmod 777 /opt/fastqc-0.11.9/fastqc
sudo ln -s /opt/fastqc-0.11.9/fastqc /usr/local/bin/fastqc-0.11.9
sudo ln -s /usr/local/bin/fastqc-0.11.9 /usr/local/bin/fastqc
rm -r ./FastQC



echo "#### Installing HISAT2"
cd ~
wget https://github.com/DaehwanKimLab/hisat2/archive/v2.2.1.tar.gz
tar -xvzf ./v2.2.1.tar.gz
rm v2.2.1.tar.gz
cd hisat2-2.2.1
make hisat2-align-s hisat2-build-s hisat2-inspect-s
sudo mkdir /opt/hisat2-2.2.1
sudo cp ./hisat2-align-s ./hisat2-build-s ./hisat2-inspect-s /opt/hisat2-2.2.1/
sudo cp ./hisat2_*.py /opt/hisat2-2.2.1/
sudo ln -s /opt/hisat2-2.2.1/hisat2-align-s /usr/local/bin/hisat2-align-s-2.2.1
sudo ln -s /usr/local/bin/hisat2-align-s-2.2.1 /usr/local/bin/hisat2-align-s
sudo ln -s /usr/local/bin/hisat2-align-s-2.2.1 /usr/local/bin/hisat2-align
sudo ln -s /opt/hisat2-2.2.1/hisat2-build-s /usr/local/bin/hisat2-build-s-2.2.1
sudo ln -s /usr/local/bin/hisat2-build-s-2.2.1 /usr/local/bin/hisat2-build-s
sudo ln -s /usr/local/bin/hisat2-build-s-2.2.1 /usr/local/bin/hisat2-build
sudo ln -s /opt/hisat2-2.2.1/hisat2-inspect-s /usr/local/bin/hisat2-inspect-s-2.2.1
sudo ln -s /usr/local/bin/hisat2-inspect-s-2.2.1 /usr/local/bin/hisat2-inspect-s
sudo ln -s /usr/local/bin/hisat2-inspect-s-2.2.1 /usr/local/bin/hisat2-inspect
sudo ln -s /opt/hisat2-2.2.1/hisat2_extract_splice_sites.py /usr/local/bin/hisat2_extract_splice_sites.py
sudo ln -s /opt/hisat2-2.2.1/hisat2_extract_exons.py /usr/local/bin/hisat2_extract_exons.py
sudo ln -s /opt/hisat2-2.2.1/hisat2_extract_snps_haplotypes_UCSC.py /usr/local/bin/hisat2_extract_snps_haplotypes_UCSC.py
sudo ln -s /opt/hisat2-2.2.1/hisat2_extract_snps_haplotypes_VCF.py /usr/local/bin/hisat2_extract_snps_haplotypes_VCF.py
cd ..
rm -r ./hisat2-2.2.1



echo "#### Installing salmon"
cd ~
sudo apt install -y libboost-all-dev libtbb-dev libssl-dev
wget https://github.com/Kitware/CMake/releases/download/v3.18.2/cmake-3.18.2.tar.gz
tar -xvzf cmake-3.18.2.tar.gz
rm cmake-3.18.2.tar.gz
cd cmake-3.18.2
./configure
make
sudo make install
cd ~
wget https://github.com/COMBINE-lab/salmon/archive/v1.3.0.tar.gz
tar -xvzf ./v1.3.0.tar.gz
rm ./v1.3.0.tar.gz
cd ./salmon-1.3.0
mkdir build
cd build
cmake ..
make
make install

cd ..
sudo mkdir /opt/salmon-1.3.0
sudo cp ./bin/salmon /opt/salmon-1.3.0/
sudo ln -s /opt/salmon-1.3.0/salmon /usr/local/bin/salmon-1.3.0
sudo ln -s /usr/local/bin/salmon-1.3.0 /usr/local/bin/salmon

cd ~
rm -r salmon-1.3.0
rm -rf cmake-3.18.2




echo "#### Installing GATK4"
cd ~
wget https://github.com/broadinstitute/gatk/releases/download/4.1.9.0/gatk-4.1.9.0.zip
unzip gatk-4.1.9.0.zip
rm gatk-4.1.9.0.zip
sudo mkdir /opt/gatk-4.1.9.0
sudo cp -r ./gatk-4.1.9.0/* /opt/gatk-4.1.9.0/
sudo ln -s /opt/gatk-4.1.9.0/gatk /usr/local/bin/gatk-4.1.9.0
sudo ln -s /usr/local/bin/gatk-4.1.9.0 /usr/local/bin/gatk
sudo rm -r ./gatk-4.1.9.0





echo "#### Installing STAR"
cd ~
wget https://github.com/alexdobin/STAR/archive/2.7.7a.tar.gz
tar -xzf 2.7.7a.tar.gz
rm 2.7.7a.tar.gz
cd STAR-2.7.7a/source
make STAR
sudo mkdir /opt/STAR-2.7.7a
sudo cp ./STAR /opt/STAR-2.7.7a/
sudo ln -s /opt/STAR-2.7.7a/STAR /usr/local/bin/STAR-2.7.7a
sudo ln -s /usr/local/bin/STAR-2.7.7a /usr/local/bin/STAR
cd ~
rm -r ./STAR-2.7.7a








echo "Installing R..."
sudo apt install -y r-base
sudo apt install -y libcurl4-openssl-dev libxml2-dev

echo "Installing psrecord for monitoring"
sudo apt install -y python-pip
pip install psrecord

echo "Fetching config files"
cd ~
git clone https://git.app.uib.no/gni042/configs.git
cd configs
bash INSTALL.sh

echo "export PATH=~/.local/bin/:$PATH" >> ~/.bashrc
