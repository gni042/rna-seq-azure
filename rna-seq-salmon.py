#!/usr/bin/env python3

import sys
import re
import os
import multiprocessing
from psutil import virtual_memory
import math
import tempfile
import subprocess
#import glob
#import time
import getopt

assert sys.version_info >= (3,5)

def usage():
    sys.stdout.write("USAGE\n")
    sys.stdout.write("      python3 rna-seq-salmon.py [OPTIONS] -o DIR -1 FILE -2 FILE\n")
    sys.stdout.write("OPTIONS\n")
    sys.stdout.write("      -o, --output PATH\n")
    sys.stdout.write("            output directory where files will be stored [REQUIRED]\n")
    sys.stdout.write("      -1 FILE\n")
    sys.stdout.write("            input fastq file (R1) [REQUIRED]\n")
    sys.stdout.write("      -2 FILE\n")
    sys.stdout.write("            input fastq file (R2) [REQUIRED]\n")
    sys.stdout.write("      -i, --sample-id SAMPLEID\n")
    sys.stdout.write("            sample id to be used in the output files. Defaults to \n")
    sys.stdout.write("            common string between fastq files\n")
    sys.stdout.write("      --salmon-indices PATH\n")
    sys.stdout.write("            path to salmon indexed transcripts\n")
    sys.stdout.write("      --fastqc-exe PATH\n")
    sys.stdout.write("            path to FastQC executable if not in PATH\n")
    sys.stdout.write("      --salmon-exe PATH\n")
    sys.stdout.write("            path to salmon executable if not in PATH\n")
    sys.stdout.write("      -h --help\n")
    sys.stdout.write("            print (this) help message\n")

output_dir = ""
fastq_files = ["",""]
sample_id = ""
index_transcripts = "./ref/salmon_1.3.0_index_gencode35"
fastqc            = "fastqc"
salmon            = "salmon"

options, remaining = getopt.getopt(sys.argv[1:],
                                   'o:1:2:i:h',
                                   ['help','output=',
                                    'sample-id=',
                                    'salmon-indices=',
                                    'fastqc-exe=',
                                    'salmon-exe=',
                                    ])

for opt, arg in options:
    if opt in ('-o', '--output'):
        output_dir = arg
    if opt in ('-1'):
        fastq_files[0] = arg
    if opt in ('-2'):
        fastq_files[1] = arg
    if opt in ('-i', '--sample-id'):
        sample_id = arg
    elif opt in ('--salmon-indices'):
        index_transcripts = arg
    elif opt in ('--fastqc-exe'):
        fastqc = arg
    elif opt in ('--salmon-exe'):
        salmon = arg
    elif opt in ('-h', '--help'):
        usage()
        sys.exit()


###############################################################################
#                                                                             #
#                                FUNCTIONS                                    #
#                                                                             #
###############################################################################

def error(msg="", exit_code=1):
    msg = " >>> ERROR <<< " + msg.rstrip() + "\n"
    sys.stdout.write(msg)
    sys.exit(exit_code)

def warning(msg=""):
    msg = " *** WARNING *** " + msg.rstrip() + "\n"
    sys.stdout.write(msg)

def run_local(cmd, lines_of_code=[], job_script_dir = os.path.join(os.getcwd(), "jobscripts"), prefix=""):
    os.makedirs(job_script_dir, exist_ok=True)
    for index, item in enumerate(lines_of_code):
        lines_of_code[index] = lines_of_code[index]+"\n"
    # Create script
    with tempfile.NamedTemporaryFile(mode='w+t', suffix='.sh', prefix=prefix, dir=job_script_dir, delete=False) as script:
        job_header = ["#!/bin/sh\n"]
        job_lines = job_header + lines_of_code + [cmd+"\n"]
        script.writelines(job_lines)
    # Submit job
    with open(os.path.join(job_script_dir, script.name + ".local.out"), "w") as out_file:
        subprocess.run("sh "+script.name, shell=True, stdout=out_file, stderr=out_file)

def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def lsubstr(s1, s2):
    answer = ""
    len1, len2 = len(s1), len(s2)
    for i in range(len1):
        match = ""
        for j in range(len2):
            if (i + j < len1 and s1[i + j] == s2[j]):
                match += s2[j]
            else:
                if (len(match) > len(answer)): answer = match
                match = ""
    return re.sub(r"^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$", "",answer)

def remove_double_ext(filename):
    path = os.path.splitext(filename)[0]
    return os.path.splitext(path)[0]



###############################################################################
#                                                                             #
#                                  TESTS                                      #
#                                                                             #
###############################################################################

# Check FASTQ files
if fastq_files[0] == "" or fastq_files[1] == "":
    error("Specify both fastq files using -1 and -2 options\n")
    sys.exit(1)
if not os.path.isfile(fastq_files[0]) or not os.path.isfile(fastq_files[1]):
    error("At least one of the fastq files provided does not exist\n")
    sys.exit(1)

# Create sample_id
if sample_id == "":
    sample_id = lsubstr(os.path.basename(fastq_files[0]), os.path.basename(fastq_files[1]))
if sample_id == "":
    error("Fastq filenames have no substring in common. Provide sample id (-id option)\n")
    exit(1)

if output_dir == "":
    error("Output dir required (-o option)\n")

sys.stdout.write("[SAMPLE ID] .... " + sample_id + "\n")
sys.stdout.write("   - FILE 1 .... " + fastq_files[0] + "\n")
sys.stdout.write("   - FILE 2 .... " + fastq_files[1] + "\n")
# Get number of processors in the machine
N_proc = multiprocessing.cpu_count()
# Get RAM in the machine
RAM = math.floor(virtual_memory().total / (1024.**3))

sys.stdout.write("[CPUs] ......... " + str(N_proc) + "\n")
sys.stdout.write("[RAM] .......... " + str(RAM) + "G\n")

# Check executables
fastqc = which(fastqc)
salmon = which(salmon)

if fastqc is None:
    error("Executable of FastQC not found\n")
    sys.exit(1)
else:
    sys.stdout.write("[FASTQC EXE] ... " + fastqc + "\n")
if salmon is None:
    error("Executable of salmon not found\n")
    sys.exit(1)
else:
    sys.stdout.write("[SALMON EXE] ... " + salmon + "\n")

# Check existence of salmon indices
if not os.path.isdir(index_transcripts) or not os.path.isfile(os.path.join(index_transcripts, "ctable.bin")):
    error("Directory \"" + index_transcripts + "\" does not exist. Provide salmon index directory using --salmon-indices option.")
else:
    index_transcripts = os.path.abspath(index_transcripts)
    sys.stdout.write("[INDICES] ...... " + index_transcripts + "\n")

# Create output directory
output_dir = os.path.abspath(output_dir)
os.makedirs(output_dir, exist_ok=True)
sys.stdout.write("[OUTPUT DIR] ... " + output_dir + "\n")

# OUTPUT DIRECTORIES
jobs_dir   = os.path.join(output_dir, sample_id, "jobscripts")
qc_dir     = os.path.join(output_dir, sample_id, "qc")
log_dir    = os.path.join(output_dir, sample_id, "logs")
salmon_dir = os.path.join(output_dir, sample_id, "salmon")

if os.path.isdir(jobs_dir):
    warning("Provided directory for job scripts already exists\n")
else:
    os.makedirs(jobs_dir)
sys.stdout.write("[JOBS DIR] ..... " + jobs_dir + "\n")




# ###############################################################################
# ###############################################################################
# #                                                                             #
# #                                 FASTQC                                      #
# #                                                                             #
# ###############################################################################
# ###############################################################################
# 
# os.makedirs(qc_dir, exist_ok=True)
# 
# sys.stdout.write("=== TASK: RUNNING FASTQC... ")
# sys.stdout.flush()
# 
# inputs = fastq_files
# outputs = [ os.path.join(qc_dir, remove_double_ext(os.path.basename(fastq_files[0])) + '_fastqc.html'),
#             os.path.join(qc_dir, remove_double_ext(os.path.basename(fastq_files[1])) + '_fastqc.html')]
# 
# if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
#     sys.stdout.write("SKIPPED\n")
# else:
#     threads = N_proc
#     cmd = "{exe} -o {qc_dir} -q -t {threads} --extract {input1} {input2}".format(
#                 exe=fastqc, 
#                 qc_dir=qc_dir,
#                 input1=inputs[0],
#                 input2=inputs[1],
#                 threads=threads
#             )
# 
#     sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="fastqc_")
#     sys.stdout.write("DONE\n")



###############################################################################
###############################################################################
#                                                                             #
#                                RUN SALMON                                   #
#                                                                             #
###############################################################################
###############################################################################

os.makedirs(salmon_dir, exist_ok=True)

sys.stdout.write("=== TASK: RUNNING SALMON... ")
sys.stdout.flush()

inputs = fastq_files
outputs = [ os.path.join(salmon_dir, 'quant.sf') ]

# Check if this id has already been salmoned
if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
    sys.stdout.write("SKIPPED\n")
else:
    threads = N_proc
    #cmd = "{exe} quant --gcBias -i {index_transcripts} -l A -1 {fqfile1} -2 {fqfile2} --validateMappings -o {outdir} --threads {threads} && gzip {sf}".format(
    cmd = "{exe} quant --gcBias -i {index_transcripts} -l A -1 {fqfile1} -2 {fqfile2} --validateMappings -o {outdir} --threads {threads}".format(
           exe=salmon,
           index_transcripts=index_transcripts,
           fqfile1=inputs[0],
           fqfile2=inputs[1],
           outdir=salmon_dir,
           threads=threads)

    sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="salmon_")
    sys.stdout.write("DONE\n")

sys.exit(0)

