#!/bin/bash

usage()
{
    echo "USAGE"
    echo "      $0 [OPTIONS]"
    echo ""
    echo "OPTIONS"
    echo "   -o DIR"
    echo "          Output directory for reference and indices (default ../ref_HISAT2)."
    echo "   -f"
    echo "          Overwrite previous reference files and/or indices, if any."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

SCRIPT_PATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"
ref_dir=${SCRIPT_PATH}/../ref_HISAT2
FORCE=0



while getopts ":o:fh" opt; do
    case ${opt} in 
        o)
            ref_dir=$OPTARG
            ;;
        f)
            FORCE=1
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done

shift $((OPTIND -1))


if [[ -e "${ref_dir}" ]]; then
   if [[ ${FORCE} == 0 ]]; then
       echo "[ERROR] Output directory (or file) \"${ref_dir}\" exists and is not empty. To overwrite, use -f option."
       exit 1
    fi
fi

echo "Download Hisat2 precompiled indices..."
mkdir -p ${ref_dir}
if [[ ! -d ${ref_dir}/grch38_snptran && ! -s ${ref_dir}/grch38_snptran.tar.gz ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        https://genome-idx.s3.amazonaws.com/hisat/grch38_snptran.tar.gz
fi
if [[ ! -s ${ref_dir}/grch38_snp_tran/genome_snp_tran.1.ht2 ]]; then
    tar -xvzf ${ref_dir}/grch38_snptran.tar.gz && rm ${ref_dir}/grch38_snptran.tar.gz
fi

echo "Donwload other reference files..."
if [[ !-s ${ref_dir}/grch38_snp_tran/make_grch38_snp_tran_fixed.sh ]]; then
    cat ${ref_dir}/grch38_snp_tran/make_grch38_snp_tran.sh | \
        sed 's/which hisat2_extract_snps_UCSC\.py/which hisat2_extract_snps_haplotypes_UCSC.py/' | \
        sed 's/CMD=.+/CMD=""/' > ${ref_dir}/grch38_snp_tran/make_grch38_snp_tran_fixed.sh
    cd ${ref_dir}/grch38_snp_tran
    bash make_grch38_snp_tran_fixed.sh
    samtools faidx genome.fa
    gatk CreateSequenceDictionary -R genome.fa -O genome.dict
    awk '$3=="exon"{print $1"\t"($4-1)"\t"$5}' Homo_sapiens.GRCh38.84.gtf > Homo_sapiens.GRCh38.84.bed
    gatk BedToIntervalList -I Homo_sapiens.GRCh38.84.bed -O Homo_sapiens.GRCh38.84.exons.interval_list -SD genome.dict
    cd -
fi

echo "Convert GTF file to CallingIntervals..."
bed_output_file=$( echo ${ref_dir}/${GTF_FILE} | sed 's/\.gtf/.bed/' )
seq_dict=$( echo ${ref_dir}/${GENOME} | sed 's/\.fa/.dict/' )
output_file=$( echo ${ref_dir}/${GTF_FILE} | sed 's/\.gtf/.exons.interval_list/' )
if [[ ! -s ${output_file} ]]; then
    ${gatk_exe} BedToIntervalList \
        -I ${bed_output_file} \
        -O ${output_file} \
        -SD ${seq_dict}
fi


# download "omni" truth set
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/1000G_omni2.5.hg38.vcf.gz ${ref_dir}/
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/1000G_omni2.5.hg38.vcf.gz.tbi ${ref_dir}/
# download "hapmap" truth set
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/hapmap_3.3.hg38.vcf.gz ${ref_dir}/
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/hapmap_3.3.hg38.vcf.gz.tbi ${ref_dir}/
# download "1000G" truth set
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz ${ref_dir}/
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz.tbi ${ref_dir}/
# download "dbsnp" truth set
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf ${ref_dir}/
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf.idx ${ref_dir}/
# download "known indels"
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.known_indels.vcf.gz ${ref_dir}/
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.known_indels.vcf.gz.tbi ${ref_dir}/
# download "mills indels"
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz ${ref_dir}/
gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi ${ref_dir}/



# echo "Downloading variation..."
# output_file=${ref_dir}/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz
# if [[ ! -s ${output_file} ]]; then
#     wget -q --directory-prefix=${ref_dir} -N \
#         http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz
#     wget -q --directory-prefix=${ref_dir} -N \
#         http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz.tbi
# fi
# 
# 
# echo "Separating SNVs from INDELS..."
# snps=${ref_dir}/snvs.vcf
# indels=${ref_dir}/indels.vcf
# if [[ ! -s ${snps} ]]; then
#     bcftools view -v snps -o ${snps} ${output_file}
# fi
# if [[ ! -s ${indels} ]]; then
#     bcftools view -v indels -o ${indels} ${output_file}
# fi
