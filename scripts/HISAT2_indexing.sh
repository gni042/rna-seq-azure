#!/bin/bash

usage()
{
    echo "USAGE"
    echo "      $0 [OPTIONS]"
    echo ""
    echo "OPTIONS"
    echo "   -o DIR"
    echo "          Output directory for reference and indices (default ../ref_HISAT2_new_indices)."
    echo "   -f"
    echo "          Overwrite previous reference files and/or indices, if any."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

SCRIPT_PATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"
ref_dir=${SCRIPT_PATH}/../ref_HISAT2
FORCE=0
GENCODE_RELEASE=35
#STAR="STAR"
gatk_exe="gatk"
#samtools="samtools"
#N_proc=$( grep -c processor /proc/cpuinfo )



while getopts ":o:fh" opt; do
    case ${opt} in 
        o)
            ref_dir=$OPTARG
            ;;
        f)
            FORCE=1
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done

shift $((OPTIND -1))


if [[ -e "${ref_dir}" ]]; then
   if [[ ${FORCE} == 0 ]]; then
       echo "[ERROR] Output directory (or file) \"${ref_dir}\" exists and is not empty. To overwrite, use -f option."
       exit 1
    fi
fi

if ! which hisat2-build ; then
    echo "Could not find hisat2-build in current directory or in PATH"
    exit 1
else
    HISAT2_BUILD_EXE=`which hisat2-build`
fi


if ! hisat2_extract_snps_haplotypes_VCF.py ; then
    echo "Could not find hisat2_extract_snps_haplotypes_UCSC.py in current directory or in PATH"
    exit 1
else
    HISAT2_SNP_SCRIPT=`which hisat2_extract_snps_haplotypes_VCF.py`
fi

if ! which hisat2_extract_splice_sites.py ; then
    echo "Could not find hisat2_extract_splice_sites.py in current directory or in PATH"
    exit 1
else
    HISAT2_SS_SCRIPT=`which hisat2_extract_splice_sites.py`
fi

if ! which hisat2_extract_exons.py ; then
    echo "Could not find hisat2_extract_exons.py in current directory or in PATH"
    exit 1
else
    HISAT2_EXON_SCRIPT=`which hisat2_extract_exons.py`
fi


echo "Downloading human reference genome..."
GENOME=GRCh38.primary_assembly.genome
if [[ ! -s ${ref_dir}/${GENOME}.fa ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${GENCODE_RELEASE}/${GENOME}.fa.gz
    echo "Decompressiong genome..."
    gunzip -f ${ref_dir}/${GENOME}.fa.gz
fi
if [[ ! -s ${ref_dir}/${GENOME}.fa.fai ]]; then
    ${samtools} faidx ${ref_dir}/${GENOME}.fa
fi
DICT_FILE=GRCh38.primary_assembly.genome.dict
if [[ ! -s ${ref_dir}/${GENOME}.dict ]]; then
    ${gatk_exe} CreateSequenceDictionary -R ${ref_dir}/${GENOME}.fa -O ${ref_dir}/${DICT_FILE}
fi

echo "Downloading Gencode human GTF annotatiowk '$3=="exon"{}n release ${GENCODE_RELEASE}..."
GTF_FILE=gencode.v${GENCODE_RELEASE}.annotation.gtf
if [[ ! -s ${ref_dir}/${GTF_FILE} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${GENCODE_RELEASE}/${GTF_FILE}.gz
    echo "Decompressing GTF..."
    gunzip -f ${ref_dir}/${GTF_FILE}.gz
fi

BED_FILE=gencode.v${GENCODE_RELEASE}.exons.bed
IL_FILE=gencode.v${GENCODE_RELEASE}.exons.interval_list
if [[ ! -s ${ref_dir}/${BED_FILE} ]]; then
    awk '$3=="exon"{print $1"\t"($4-1)"\t"$5}' ${ref_dir}/${GTF_FILE} > ${ref_dir}/${BED_FILE}
    ${gatk_exe} BedToIntervalList -I ${ref_dir}/${BED_FILE} -O ${ref_dir}/${IL_FILE} -SD ${ref_dir}/${DICT_FILE}

echo "Downloading variation..."
VCF_VAR_COMMON="00-common_all.vcf"
if [[ ! -s ${ref_dir}/${VCF_VAR_COMMON} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/${VCF_VAR_COMMON}.gz
    # fix header
    zcat ${ref_dir}/${VCF_VAR_COMMON}.gz | awk 'BEGIN{FS="\t";OFS="\t"}{if(/^#/)print;else {gsub("MT", "M", $1) ; print "chr"$0}}' > ${ref_dir}/${VCF_VAR_COMMON}
    rm ${ref_dir}/${VCF_VAR_COMMON}.gz
    ${gatk_exe} IndexFeatureFile -I ${ref_dir}/${VCF_VAR_COMMON}
fi

VCF_VAR_ALL="00-All.vcf"
if [[ ! -s ${ref_dir}/${VCF_VAR_ALL} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/${VCF_VAR_ALL}.gz
    # fix header
    zcat ${ref_dir}/${VCF_VAR_ALL}.gz | awk 'BEGIN{FS="\t";OFS="\t"}{if(/^#/)print;else {gsub("MT", "M", $1) ; print "chr"$0}}' > ${ref_dir}/${VCF_VAR_ALL}
    rm ${ref_dir}/${VCF_VAR_ALL}.gz
    ${gatk_exe} IndexFeatureFile -I ${ref_dir}/${VCF_VAR_ALL}
fi

SNV_INDELS_1KG="ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf"
if [[ ! -s ${ref_dir}/${SNV_INDELS_1KG} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/${SNV_INDELS_1KG}.gz
    zcat ${ref_dir}/${SNV_INDELS_1KG}.gz | awk 'BEGIN{FS="\t";OFS="\t"}{if(/^#/)print;else {gsub("MT", "M", $1) ; print "chr"$0}}' | grep -v '^##contig' > ${ref_dir}/${SNV_INDELS_1KG}
    rm ${ref_dir}/${SNV_INDELS_1KG}.gz
    ${gatk_exe} IndexFeatureFile -I ${ref_dir}/${SNV_INDELS_1KG}
fi

CLINVAR="clinvar_20210221.vcf".gz
if [[ ! -s ${ref_dir}/${CLINVAR} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
    ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/${CLINVAR}.gz
    zcat ${CLINVAR}.gz | 'BEGIN{FS="\t";OFS="\t"}{if(/^#/)print;else {gsub("MT", "M", $1) ; print "chr"$0}}' > ${ref_dir}/${CLINVAR}
    rm ${ref_dir}/${CLINVAR}.gz
    ${gatk_exe} IndexFeatureFile -I ${ref_dir}/${CLINVAR}
fi

echo "Generating intermediate files for indexing..."
if [[ ! -s ${ref_dir}/genome.snp ]]; then
    ${HISAT2_SNP_SCRIPT} ${ref_dir}/${GENOME}.fa ${ref_dir}/${VCF_VAR} genome
fi

if [[ ! -s ${ref_dir}/genome.ss ]]; then
    ${HISAT2_SS_SCRIPT} ${GTF_FILE} > ${ref_dir}/genome.ss
fi

if [[ ! -s ${ref_dir}/genome.exon ]]; then
    ${HISAT2_EXON_SCRIPT} ${GTF_FILE} > ${ref_dir}/genome.exon
fi

#### THIS NEEDS 200GB RAM!!!!

CMD="${HISAT2_BUILD_EXE} -p 8 ${ref_dir}/${GENOME}.fa --snp ${ref_dir}/genome.snp --haplotype ${ref_dir}/genome.haplotype --ss ${ref_dir}/genome.ss --exon ${ref_dir}/genome.exon ${ref_dir}/genome_snp_tran"
echo Running $CMD
if $CMD ; then
	echo "genome index built; you may remove fasta files"
else
	echo "Index building failed; see error message"
fi






