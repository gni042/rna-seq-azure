#!/bin/bash

usage()
{
    echo "USAGE"
    echo "      $0 [OPTIONS]"
    echo ""
    echo "OPTIONS"
    echo "   -o DIR"
    echo "          Output directory for reference and indices (default ../ref)."
    echo "   -f"
    echo "          Overwrite previous reference files and/or indices, if any."
    echo "   -r INTEGER"
    echo "          Gencode release (integer; default 35)."
    echo "   -s PATH"
    echo "          STAR executable path (default STAR)."
    echo "   -g PATH"
    echo "          GATK executable path (default gatk)."
    echo "   -m PATH"
    echo "          Samtools executable path (default samtools)."
    echo "   -t INTEGER"
    echo "          Number of threads for the indexing (defaults to all cpus)."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

SCRIPT_PATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"
ref_dir=${SCRIPT_PATH}/../ref_STAR
FORCE=0
GENCODE_RELEASE=35
STAR="STAR"
gatk_exe="gatk"
samtools="samtools"
N_proc=$( grep -c processor /proc/cpuinfo )
#RAM=$( free -g | grep '^Mem' | awk '{print $2}' )



while getopts ":o:fr:s:g:m:t:h" opt; do
    case ${opt} in 
        o)
            ref_dir=$OPTARG
            ;;
        f)
            FORCE=1
            ;;
        r)
            GENCODE_RELEASE=$OPTARG
            ;;
        s)
            STAR=$OPTARG
            ;;
        g)
            gatk_exe=$OPTARG
            ;;
        m)
            samtools=$OPTARG
            ;;
        t)
            N_proc=$OPTARG
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done

shift $((OPTIND -1))

STAR=$( which $STAR )
if [[ $STAR == "" ]]; then
    echo "[ERROR] STAR executable not found. Provide executable with -s option."
    exit 1
fi
gatk_exe=$( which $gatk_exe )
if [[ $gatk_exe == "" ]]; then
    echo "[ERROR] GATK executable not found. Provide executable with -g option."
    exit 1
fi
samtools=$( which $samtools )
if [[ $samtools == "" ]]; then
    echo "[ERROR] Samtools executable not found. Provide executable with -m option."
    exit 1
fi

# 1) Download reference
if [[ -e "${ref_dir}" ]]; then
   if [[ ${FORCE} == 0 ]]; then
       echo "[ERROR] Output directory (or file) \"${ref_dir}\" exists and is not empty. To overwrite, use -f option."
       exit 1
    fi
fi

echo "Downloading human reference genome..."
GENOME=GRCh38.primary_assembly.genome.fa
if [[ ! -s ${ref_dir}/${GENOME} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${GENCODE_RELEASE}/${GENOME}.gz
    echo "Decompressiong genome..."
    gunzip -f ${ref_dir}/${GENOME}.gz
fi
if [[ ! -s ${ref_dir}/${GENOME}.fai ]]; then
    ${samtools} faidx ${ref_dir}/${GENOME}
fi

echo "Downloading Gencode human GTF annotation release ${GENCODE_RELEASE}..."
GTF_FILE=gencode.v${GENCODE_RELEASE}.annotation.gtf
if [[ ! -s ${ref_dir}/${GTF_FILE} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${GENCODE_RELEASE}/${GTF_FILE}.gz
    echo "Decompressing GTF..."
    gunzip -f ${ref_dir}/${GTF_FILE}.gz
fi


echo "STAR indexing (${N_proc} threads)..."
mkdir -p ${ref_dir}/indices
if [[ ! -s ${ref_dir}/indices/SA ]]; then
    ${STAR} --runThreadN ${N_proc} \
            --runMode genomeGenerate \
            --genomeDir ${ref_dir}/indices \
            --genomeFastaFiles ${ref_dir}/${GENOME} \
            --sjdbGTFfile ${ref_dir}/${GTF_FILE} \
            --sjdbOverhang 100
fi


echo "Convert GTF file to CallingIntervals..."
bed_output_file=$( echo ${ref_dir}/${GTF_FILE} | sed 's/\.gtf/.bed/' )
seq_dict=$( echo ${ref_dir}/${GENOME} | sed 's/\.fa/.dict/' )
output_file=$( echo ${ref_dir}/${GTF_FILE} | sed 's/\.gtf/.exons.interval_list/' )
if [[ ! -s ${bed_output_file} ]]; then
    awk '$3=="exon"{print $1"\t"($4-1)"\t"$5}' ${ref_dir}/${GTF_FILE} > ${bed_output_file}
fi
if [[ ! -s ${seq_dict} ]]; then
    ${gatk_exe} CreateSequenceDictionary \
        -R ${ref_dir}/${GENOME} \
        -O ${seq_dict}
fi
if [[ ! -s ${output_file} ]]; then
    ${gatk_exe} BedToIntervalList \
        -I ${bed_output_file} \
        -O ${output_file} \
        -SD ${seq_dict}
fi


echo "Downloading variation..."
output_file=${ref_dir}/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz
if [[ ! -s ${output_file} ]]; then
    wget -q --directory-prefix=${ref_dir} -N \
        http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz
    wget -q --directory-prefix=${ref_dir} -N \
        http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz.tbi
fi


echo "Separating SNVs from INDELS..."
