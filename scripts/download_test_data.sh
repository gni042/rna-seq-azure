#!/bin/bash

SCRIPT_PATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"

echo "Downloading and extracting HBR samples..."
mkdir -p ${SCRIPT_PATH}/../test_data
wget -c -q http://genomedata.org/rnaseq-tutorial/HBR_UHR_ERCC_ds_5pc.tar -O - | \
    tar -x -C ${SCRIPT_PATH}/../test_data/
for f1 in `find ${SCRIPT_PATH}/../test_data/ -name *.fastq.gz`; do
    f2=$( echo ${f1} | sed 's/\.read1\.fastq\.gz$/.R1.fastq.gz/' )
    f2=$( echo ${f2} | sed 's/\.read2\.fastq\.gz$/.R2.fastq.gz/' )
    mv ${f1} ${f2}
done
