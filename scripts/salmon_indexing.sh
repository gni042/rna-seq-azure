#!/bin/bash

usage()
{
    echo "USAGE"
    echo "      $0 [OPTIONS]"
    echo ""
    echo "OPTIONS"
    echo "   -o DIR"
    echo "          Output directory for reference and indices (default ../ref)."
    echo "   -f"
    echo "          Overwrite previous reference files and/or indices, if any."
    echo "   -r INTEGER"
    echo "          Gencode release (integer; default 35)."
    echo "   -s PATH"
    echo "          Salmon executable path (default salmon)."
    echo "   -t INTEGER"
    echo "          Number of threads for the indexing (defaults to all cpus)."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

SCRIPT_PATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"
ref_dir=${SCRIPT_PATH}/../ref
FORCE=0
GENCODE_RELEASE=35
SALMON="salmon"
N_proc=$( grep -c processor /proc/cpuinfo )
RAM=$( free -g | grep '^Mem' | awk '{print $2}' )


while getopts ":o:fr:s:t:h" opt; do
    case ${opt} in 
        o)
            ref_dir=$OPTARG
            ;;
        f)
            FORCE=1
            ;;
        r)
            GENCODE_RELEASE=$OPTARG
            ;;
        s)
            SALMON=$OPTARG
            ;;
        t)
            N_proc=$OPTARG
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done

shift $((OPTIND -1))

SALMON=$( which $SALMON )
if [[ $SALMON == "" ]]; then
    echo "[ERROR] Salmon executable not found. Provide executable with -s option."
    exit 1
fi
salmon_version=$( $SALMON --version | cut -f 2 -d ' ' )
if [[ $salmon_version == 0.* ]]; then
    echo "[ERROR] Salmon needs to be version 1.0.0 or above."
    exit 1
else
    echo "Salmon version: $salmon_version"
fi

# 1) Download reference
if [[ ! -d "${ref_dir}" ]] || [[ -z `ls -A ${ref_dir}` ]]; then
   if [[ ${FORCE} == 0 ]]; then
       echo "[ERROR] Output directory \"${ref_dir}\" exists and is not empty. To overwrite, use -f option."
       exit 1
    fi
fi

echo "Downloading Gencode human transcriptom release ${GENCODE_RELEASE}..."
TRANSCRIPTOME=gencode.v${GENCODE_RELEASE}.transcripts.fa.gz
wget -q --directory-prefix=${ref_dir} -N \
    ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${GENCODE_RELEASE}/${TRANSCRIPTOME}

echo "Downloading human reference genome..."
GENOME=GRCh38.primary_assembly.genome.fa.gz
wget -q --directory-prefix=${ref_dir} -N \
    ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_${GENCODE_RELEASE}/${GENOME}

echo "Preparing metadata..."
grep "^>" <(gunzip -c ${ref_dir}/${GENOME}) | cut -d " " -f 1 \
    > ${ref_dir}/v${GENCODE_RELEASE}_decoys.txt
sed -i.bak -e 's/>//g' ${ref_dir}/v${GENCODE_RELEASE}_decoys.txt
cat ${ref_dir}/${TRANSCRIPTOME} ${ref_dir}/${GENOME} \
    > ${ref_dir}/v${GENCODE_RELEASE}_gentrome.fa.gz

# Indexing needs a lot of RAM. 32G were not enough!

threads=1

echo "Salmon indexing (${threads} threads)..."
${SALMON} index -t ${ref_dir}/v${GENCODE_RELEASE}_gentrome.fa.gz \
    -d ${ref_dir}/v${GENCODE_RELEASE}_decoys.txt -p ${threads} \
    -i ${ref_dir}/salmon_${salmon_version}_index_gencode${GENCODE_RELEASE} \
    --gencode

