#!/usr/bin/env python3

import sys
import re
import os
import multiprocessing
from psutil import virtual_memory
import math
import tempfile
import subprocess
import shutil
from datetime import datetime
#import glob
#import time
import getopt

assert sys.version_info >= (3,5)

def usage():
    sys.stdout.write("USAGE\n")
    sys.stdout.write("      python3 snv.py [OPTIONS] -o DIR -1 FILE -2 FILE\n")
    sys.stdout.write("OPTIONS\n")
    sys.stdout.write("      -o, --output PATH\n")
    sys.stdout.write("            output directory where files will be stored [REQUIRED]\n")
    sys.stdout.write("      -1 FILE\n")
    sys.stdout.write("            input fastq file (R1) [REQUIRED]\n")
    sys.stdout.write("      -2 FILE\n")
    sys.stdout.write("            input fastq file (R2) [REQUIRED]\n")
    sys.stdout.write("      -i, --sample-id SAMPLEID\n")
    sys.stdout.write("            sample id to be used in the output files. Defaults to \n")
    sys.stdout.write("            common string between fastq files\n")
    sys.stdout.write("      --hisat-exe PATH\n")
    sys.stdout.write("            path to hisat2-align-s executable if not in PATH\n")
    sys.stdout.write("      --hisat-reference PATH\n")
    sys.stdout.write("            path to genome reference (fasta)\n")
    sys.stdout.write("      --hisat-indices PATH\n")
    sys.stdout.write("            path to hisat2 indexed transcripts\n")
    sys.stdout.write("      --samtools-exe PATH\n")
    sys.stdout.write("            path to samtools executable if not in PATH\n")
    sys.stdout.write("      --gatk-exe PATH\n")
    sys.stdout.write("            path to GATK executable if not in PATH\n")
    sys.stdout.write("      -h --help\n")
    sys.stdout.write("            print (this) help message\n")

output_dir = ""
fastq_files = ["",""]
sample_id = ""
index_transcripts_dir = "./ref_HISAT2"
ref_fasta = "./ref_HISAT2/GRCh38.primary_assembly.genome.fa"
#dbsnp = "./ref_HISAT2/00-All.vcf"
known_variation="./ref_HISAT2/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf"
exons_interval_list="./ref_HISAT2/gencode.v35.exons.interval_list"
clinvar="./ref_HISAT2/clinvar_20210221.vcf"
hisat_exe = "hisat2-align-s"
samtools_exe = "samtools"
gatk_exe = "gatk"

options, remaining = getopt.getopt(sys.argv[1:],
                                   'o:1:2:i:h',
                                   ['help','output=',
                                    'sample-id=',
                                    'hisat-exe='
                                    'hisat-reference=',
                                    'hisat-indices=',
                                    'samtools-exe=',
                                    'gatk-exe=',
                                    ])

for opt, arg in options:
    if opt in ('-o', '--output'):
        output_dir = arg
    elif opt in ('-1'):
        fastq_files[0] = arg
    elif opt in ('-2'):
        fastq_files[1] = arg
    elif opt in ('-i', '--sample-id'):
        sample_id = arg
    elif opt in ('--hisat-exe'):
        hisat_exe = arg
    elif opt in ('--hisat-reference'):
        ref_fasta = arg
    elif opt in ('--hisat-indices'):
        index_transcripts_dir = arg
    elif opt in ('--samtools-exe'):
        samtools_exe = arg
    elif opt in ('--gatk-exe'):
        gatk_exe = arg
    elif opt in ('-h', '--help'):
        usage()
        sys.exit()
    else:
        sys.stderr.write("Unrecognized option \"" + opt + "\"\n")
        usage()
        sys.exit()


###############################################################################
#                                                                             #
#                                FUNCTIONS                                    #
#                                                                             #
###############################################################################

def error(msg="", exit_code=1):
    msg = " >>> ERROR <<< " + msg.rstrip() + "\n"
    sys.stdout.write(msg)
    sys.exit(exit_code)

def warning(msg=""):
    msg = " *** WARNING *** " + msg.rstrip() + "\n"
    sys.stdout.write(msg)

def run_local(cmd, lines_of_code=[], job_script_dir = os.path.join(os.getcwd(), "jobscripts"), prefix=""):
    os.makedirs(job_script_dir, exist_ok=True)
    for index, item in enumerate(lines_of_code):
        lines_of_code[index] = lines_of_code[index]+"\n"
    # Create script
    with tempfile.NamedTemporaryFile(mode='w+t', suffix='.sh', prefix=prefix, dir=job_script_dir, delete=False) as script:
        job_header = ["#!/bin/sh\n"]
        job_lines = job_header + lines_of_code + [cmd+"\n"]
        script.writelines(job_lines)
    # Submit job
    with open(os.path.join(job_script_dir, script.name + ".local.out"), "w") as out_file:
        subprocess.run("sh "+script.name, shell=True, stdout=out_file, stderr=out_file)

def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def lsubstr(s1, s2):
    answer = ""
    len1, len2 = len(s1), len(s2)
    for i in range(len1):
        match = ""
        for j in range(len2):
            if (i + j < len1 and s1[i + j] == s2[j]):
                match += s2[j]
            else:
                if (len(match) > len(answer)): answer = match
                match = ""
    return re.sub(r"^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$", "",answer)

def remove_double_ext(filename):
    path = os.path.splitext(filename)[0]
    return os.path.splitext(path)[0]

def durationStr(start, end):
    delta = end - start
    return("{hours} hours, {minutes} minutes, {seconds} seconds".format(
        hours=delta.days*24+delta.seconds//3600,
        minutes=(delta.seconds%3600)//60,
        seconds=(delta.seconds%3600)%60))


###############################################################################
#                                                                             #
#                                  TESTS                                      #
#                                                                             #
###############################################################################

# Check FASTQ files
if fastq_files[0] == "" or fastq_files[1] == "":
    error("Specify both fastq files using -1 and -2 options\n")
    sys.exit(1)
if not os.path.isfile(fastq_files[0]) or not os.path.isfile(fastq_files[1]):
    error("At least one of the fastq files provided does not exist\n")
    sys.exit(1)

# Create sample_id
if sample_id == "":
    sample_id = lsubstr(os.path.basename(fastq_files[0]), os.path.basename(fastq_files[1]))
if sample_id == "":
    error("Fastq filenames have no substring in common. Provide sample id (-id option)\n")
    exit(1)

if output_dir == "":
    error("Output dir required (-o option)\n")

sys.stdout.write("[SAMPLE ID] .... " + sample_id + "\n")
sys.stdout.write("   - FILE 1 .... " + fastq_files[0] + "\n")
sys.stdout.write("   - FILE 2 .... " + fastq_files[1] + "\n")
# Get number of processors in the machine
N_proc = multiprocessing.cpu_count()
# Get RAM in the machine
RAM = math.floor(virtual_memory().total / (1024.**3))

sys.stdout.write("[CPUs] ......... " + str(N_proc) + "\n")
sys.stdout.write("[RAM] .......... " + str(RAM) + "G\n")

# Check executables
hisat_exe = which(hisat_exe)
gatk_exe = which(gatk_exe)
samtools_exe = which(samtools_exe)

if hisat_exe is None:
    error("Executable of hisat2-align-2 not found\n")
    sys.exit(1)
else:
    sys.stdout.write("[HISAT2 EXE] ... " + hisat_exe + "\n")
if samtools_exe is None:
    error("Executable of samtools not found\n")
else:
    sys.stdout.write("[SAMTOOLS EXE] . " + samtools_exe + "\n")
if gatk_exe is None:
    error("Executable of GATK not found\n")
    sys.exit(1)
else:
    sys.stdout.write("[GATK EXE] ..... " + gatk_exe + "\n")

# Check existence of genome fasta reference
if not os.path.isfile(ref_fasta):
    error("Reference for genome not found in \"" + ref_fasta + "\". Provide fasta reference file --hisat-reference option.")
else:
    ref_fasta = os.path.abspath(ref_fasta)
    sys.stdout.write("[FASTA] ........ " + ref_fasta + "\n")

# Check existence of HISAT2 indices
if not os.path.isdir(index_transcripts_dir) or not os.path.isfile(os.path.join(index_transcripts_dir, "genome_snp_tran.1.ht2")):
    error("Directory \"" + index_transcripts_dir + "\" does not exist. Provide HISAT2 index directory using --hisat-indices option.")
else:
    index_transcripts_dir = os.path.abspath(index_transcripts_dir)
    sys.stdout.write("[INDICES] ...... " + index_transcripts_dir + "\n")

# Create output directory
output_dir = os.path.abspath(output_dir)
os.makedirs(output_dir, exist_ok=True)
sys.stdout.write("[OUTPUT DIR] ... " + output_dir + "\n")

# OUTPUT DIRECTORIES
jobs_dir   = os.path.join(output_dir, sample_id, "jobscripts")
log_dir    = os.path.join(output_dir, sample_id, "logs")
hisat_dir = os.path.join(output_dir, sample_id, "HISAT2")

if os.path.isdir(jobs_dir):
    warning("Provided directory for job scripts already exists\n")
else:
    os.makedirs(jobs_dir)
sys.stdout.write("[JOBS DIR] ..... " + jobs_dir + "\n")






###############################################################################
###############################################################################
#                                                                             #
#                                RUN HISAT2                                   #
#                                                                             #
###############################################################################
###############################################################################

os.makedirs(hisat_dir, exist_ok=True)

sys.stdout.write("=== TASK: RUNNING HISAT2... ")
sys.stdout.flush()

inputs = fastq_files
outputs = [ os.path.join(hisat_dir, 'aln.srt.bam') ]

# Check if this id has already been aligned with Hisat2
if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
    sys.stdout.write("SKIPPED\n")
else:
    sys.stdout.write("RUNNING\n")
    startTime = datetime.now()
    sys.stdout.write("  > STARTED: " + startTime.ctime() + "\n")
    #threads = str(N_proc)
    threads = 4
    tmp_dir = tempfile.mkdtemp(dir="/mnt")
    #print("TMPDIR = " + tmp_dir)
    abs_fq1 = os.path.abspath(inputs[0])
    abs_fq2 = os.path.abspath(inputs[1])
    cmd = "echo \"## Copying indices to {tmp_dir}/\" \n\
           cp {indices_dir}/genome_snp_tran.?.ht2 {tmp_dir}/ \n\
           echo \"## Copying input fastqfiles to {tmp_dir}/\" \n\
           zcat {fqfile1} > {local_fqfile1} \n\
           zcat {fqfile2} > {local_fqfile2} \n\
           cd {tmp_dir} \n\
           echo \"## Running Hisat2\" \n\
           {exe} -x {local_indices} \
           -1 {local_fqfile1} -2 {local_fqfile2} \
           -S {tmp_dir}/aln.sam \
           --rna-strandness RF \
           --rg-id 1 --rg LB:bar --rg PL:illumina --rg SM:{sample_id} --rg PU:unit1\n\
           {samtools_exe} view -b -h -@ 4 {tmp_dir}/aln.sam | {samtools_exe} sort -@ 4 > {tmp_dir}/aln.srt.bam \n\
            echo \"## Copying alignemnt file from {tmp_dir}/ to {hisat_dir}/\" \n\
            cp {tmp_dir}/aln.srt.bam {hisat_dir}".format(
           tmp_dir=tmp_dir,
           local_indices=os.path.join(tmp_dir, "genome_snp_tran"),
           indices_dir=index_transcripts_dir,
           exe=hisat_exe,
           sample_id=sample_id,
           samtools_exe=samtools_exe,
           threads=threads,
           fqfile1=abs_fq1,
           fqfile2=abs_fq2,
           local_fqfile1=os.path.join(tmp_dir, "01.fastq"),
           local_fqfile2=os.path.join(tmp_dir, "02.fastq"),
           hisat_dir=hisat_dir)

    sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="hisat2_")
    shutil.rmtree(tmp_dir)
    endTime = datetime.now()
    sys.stdout.write("  > FINISHED: " + endTime.ctime() + "\n")
    sys.stdout.write("  > DURATION: " + durationStr(startTime, endTime) +"\n")
    sys.stdout.write(" DONE :D\n")






###############################################################################
##############################################################################
#                                                                             #
#                              MARK DUPLICATES                                #
#                                                                             #
###############################################################################
###############################################################################


sys.stdout.write("=== TASK: DEDUP... ")
sys.stdout.flush()

inputs = [ os.path.join(hisat_dir, 'aln.srt.bam') ]
outputs = [ os.path.join(hisat_dir, 'aln.srt.dedup.bam'),
            os.path.join(hisat_dir, 'aln.srt.dedup.metrics') ]

# Check if this id has already been already deduplicated
if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
    sys.stdout.write("SKIPPED\n")
else:
    sys.stdout.write("RUNNING\n")
    startTime = datetime.now()
    sys.stdout.write("  > STARTED: " + startTime.ctime() + "\n")
    threads = str(N_proc)
    #os.rmdir(tmp_dir)
    cmd = "{exe} MarkDuplicates \
            --INPUT {input_bam} \
            --OUTPUT {base_name}.bam \
            --CREATE_INDEX true \
            --VALIDATION_STRINGENCY SILENT \
            --METRICS_FILE {base_name}.metrics".format(
           exe=gatk_exe,
           input_bam=inputs[0],
           base_name=os.path.join(hisat_dir, "aln.srt.dedup"))

    sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="dedup_")
    endTime = datetime.now()
    sys.stdout.write("  > FINISHED: " + endTime.ctime() + "\n")
    sys.stdout.write("  > DURATION: " + durationStr(startTime, endTime) +"\n")
    sys.stdout.write(" DONE :D\n")






###############################################################################
###############################################################################
#                                                                             #
#                            SPLIT N CIGAR READS                              #
#                                                                             #
###############################################################################
###############################################################################


sys.stdout.write("=== TASK: SPLIT N CIGAR READS... ")
sys.stdout.flush()

inputs = [ os.path.join(hisat_dir, 'aln.srt.dedup.bam') ]
outputs = [ os.path.join(hisat_dir, 'aln.srt.dedup.split.bam') ]

# Check if this id has N Cigar reads have already been splitted
if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
    sys.stdout.write("SKIPPED\n")
else:
    sys.stdout.write("RUNNING\n")
    startTime = datetime.now()
    sys.stdout.write("  > STARTED: " + startTime.ctime() + "\n")
    threads = str(N_proc)
    #os.rmdir(tmp_dir)
    cmd = "{exe} SplitNCigarReads \
            -R {ref_fasta} \
            -I {input_bam} \
            -O {output_bam}".format(
           exe=gatk_exe,
           ref_fasta=ref_fasta,
           input_bam=inputs[0],
           output_bam=outputs[0])

    sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="split_n_cigar_")
    endTime = datetime.now()
    sys.stdout.write("  > FINISHED: " + endTime.ctime() + "\n")
    sys.stdout.write("  > DURATION: " + durationStr(startTime, endTime) +"\n")
    sys.stdout.write(" DONE :D\n")





################################################################################
################################################################################
##                                                                             #
##                            ADD chr TO REF                                   #
##                                                                             #
################################################################################
################################################################################
#
#
#sys.stdout.write("=== TASK: ADD \"chr\" TO REFERENCE... ")
#sys.stdout.flush()
#
#inputs = [ os.path.join(hisat_dir, 'aln.srt.dedup.split.bam') ]
#outputs = [ os.path.join(hisat_dir, 'aln.srt.dedup.split.clean.bam') ]
#
## Check if this id has been cleaned
#if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
#    sys.stdout.write("SKIPPED\n")
#else:
#    sys.stdout.write("RUNNING\n")
#    startTime = datetime.now()
#    sys.stdout.write("  > STARTED: " + startTime.ctime() + "\n")
#    threads = str(N_proc)
#
#    cmd = "{gatk_exe} AddOrReplaceReadGroups \
#            -I {tmp_sam} \
#            -O {output_bam} \
#            -RGLB bar \
#            -RGPL illumina \
#            -RGPU unit1 \
#            -RGSM Sample1 \
#            -CREATE_INDEX True \n\
#           rm {tmp_sam}".format(
#           samtools_exe=samtools_exe,
#           gatk_exe=gatk_exe,
#           input_bam=inputs[0],
#           output_bam=outputs[0],
#           tmp_sam=inputs[0]+".temp.sam")
#
#    sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="add_chr_")
#    endTime = datetime.now()
#    sys.stdout.write("  > FINISHED: " + endTime.ctime() + "\n")
#    sys.stdout.write("  > DURATION: " + durationStr(startTime, endTime) +"\n")
#    sys.stdout.write(" DONE :D\n")
#
#
#
#sys.exit(1)




###############################################################################
###############################################################################
#                                                                             #
#                            VARIANT CALLING                                  #
#                                                                             #
###############################################################################
###############################################################################


sys.stdout.write("=== TASK: VARIANT CALLING... ")
sys.stdout.flush()

inputs = [ os.path.join(hisat_dir, 'aln.srt.dedup.split.bam') ]
outputs = [ os.path.join(hisat_dir, 'recal_data.csv'),
            os.path.join(hisat_dir, 'aln.srt.dedup.split.bqsr.bam'),
            os.path.join(hisat_dir, 'variants.vcf.gz'),
            os.path.join(hisat_dir, 'variants_filtered.vcf.gz') ]

# Check if this id has been already recalibrated
if sum([ os.path.isfile(x) for x in outputs ]) == len(outputs):
    sys.stdout.write("SKIPPED\n")
else:
    sys.stdout.write("RUNNING\n")
    startTime = datetime.now()
    sys.stdout.write("  > STARTED: " + startTime.ctime() + "\n")
    threads = str(N_proc)
    tmp_dir = tempfile.mkdtemp(dir="/mnt")
    #os.rmdir(tmp_dir)

    cmd = "echo \"## Copying reference to {local_ref}\" \n\
           cp {ref_fasta} {local_ref} \n\
           cp {ref_fasta}.fai {local_ref}.fai \n\
           cp {ref_fasta_dict} {local_ref_fasta_dict} \n\
           echo \"## Copying bamfile\"\n\
           cp {input_bam} {local_bam} \n\
           echo \"## Copying variation files\" \n\
           cp {interval_list} {local_interval_list} \n\
           cp {known_variation} {local_known_variation} \n\
           cp {known_variation}.idx {local_known_variation}.idx \n\
           cp {clinvar} {local_clinvar} \n\
           cp {clinvar}.idx {local_clinvar}.idx \n\
           cd {tmp_dir} \n\
           {gatk_exe} BaseRecalibrator \
            -R {local_ref} \
            -I {local_bam} \
            --use-original-qualities \
            -O {recal_output_file} \
            --known-sites {local_known_variation} \n\
           cp {recal_output_file} {hisat_output_dir}/ \n\
           {gatk_exe} ApplyBQSR \
            --add-output-sam-program-record \
            -R {local_ref} \
            -I {local_bam} \
            --use-original-qualities \
            -O {final_bam} \
            --bqsr-recal-file {recal_output_file} \n\
           cp {final_bam} {hisat_output_dir}/ \n\
           {gatk_exe} HaplotypeCaller \
            -R {local_ref} \
            -I {final_bam} \
            -L {local_interval_list} \
            -O {output_vcf} \
            -comp:CLINVAR {local_clinvar} \
            -dont-use-soft-clipped-bases \
            --standard-min-confidence-threshold-for-calling 20 \n\
           cp {output_vcf} {output_vcf}.tbi {hisat_output_dir}/ \n\
           {gatk_exe} VariantFiltration \
            -R {local_ref} \
            -V {output_vcf} \
            --window 35 --cluster 3 \
            --filter-name \"FS\" --filter \"FS > 30.0\" \
            --filter-name \"QD\" --filter \"QD < 2.0\" \
            -O {filtered_vcf} \n\
           cp {filtered_vcf} {hisat_output_dir}/".format(
           tmp_dir=tmp_dir,
           ref_fasta=ref_fasta,
           local_ref=os.path.join(tmp_dir, "genome.fa"),
           ref_fasta_dict=re.sub("\.fa$|\.fasta$", ".dict", ref_fasta),
           local_ref_fasta_dict=re.sub("\.fa$|\.fasta$",".dict", os.path.join(tmp_dir, "genome.fa")),
           samtools_exe=samtools_exe,
           gatk_exe=gatk_exe,
           input_bam=inputs[0],
           local_bam=os.path.join(tmp_dir, "aln.bam"),
           known_variation=known_variation,
           local_known_variation=os.path.join(tmp_dir, "known_variation.vcf"),
           recal_output_file=os.path.join(tmp_dir, "recal_data.csv"),
           final_bam=os.path.join(tmp_dir, "aln.srt.dedup.split.bqsr.bam"),
           interval_list=exons_interval_list,
           local_interval_list=os.path.join(tmp_dir, "exons.interval_list"),
           output_vcf=os.path.join(tmp_dir, "variants.vcf.gz"),
           clinvar=clinvar,
           local_clinvar=os.path.join(tmp_dir, "clinvar.vcf"),
           filtered_vcf=os.path.join(tmp_dir, "variants_filtered.vcf.gz"),
           hisat_output_dir=hisat_dir)

    sp = run_local(cmd=cmd, lines_of_code=[], job_script_dir=jobs_dir, prefix="variant_calling_")
    endTime = datetime.now()
    sys.stdout.write("  > FINISHED: " + endTime.ctime() + "\n")
    sys.stdout.write("  > DURATION: " + durationStr(startTime, endTime) +"\n")
    sys.stdout.write(" DONE :D\n")
    sys.stdout.write("DONE\n")
    shutil.rmtree(tmp_dir)



sys.exit(0)

